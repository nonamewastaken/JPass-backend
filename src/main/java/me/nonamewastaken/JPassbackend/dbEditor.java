package me.nonamewastaken.JPassbackend;

import java.sql.*;

public class dbEditor {
    static Connection con;
    static String emailCriteria = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";

    ///////////////////////////////////////////////////////////////////
    //Function, that establishes the connection to the mysql database//
    ///////////////////////////////////////////////////////////////////

    //requires the db ip & path the username and password
    public static void establishConnection(String db, String user, String Password) {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver"); // loads Driver
            con = DriverManager.getConnection(db, user, Password); // establish connection
            System.out.println("Database access granted!");

        } catch (Exception e) {
            System.out.println("Database access denied!");
            e.printStackTrace();
        }

    }
    ////////////////////////////////////
    //adds user to to be emailCodes db//
    ////////////////////////////////////

    //only works if checkUserPresence() returns negative ==> no duplicate users
    public static String addEmailCodeToDB(String email, String code){
        if(email.matches(emailCriteria)) { //checks for valid email syntax
            if(!checkUserPresenceInValidation(email)){ //checks if user already requested a code
                try {
                    Statement stm = con.createStatement(); //create statement
                    String dbop = "INSERT INTO users.emailCodes VALUES ('" + email + "', '" + code + "');";
                    stm.execute(dbop); //run previously defined command on mysql (writes user & key to db)
                    stm.close();
                    System.out.println("added user to verification");
                    return "success!";
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("error adding user to verification");
                    return "Internal server error!"; //returns internal server error if code breaks
                }

            }else{
                return "resend code";
            }
        }else{
            return "invalid email";
        }

    }


    //////////////////////////////////////////
    //function to add a user to the database//
    //////////////////////////////////////////

    //only works if checkUserPresence() returns negative ==> no duplicate users
    public static String addUser(String name, String key, String code){
        if(name.matches(emailCriteria) && code.equals(getEmailCode(name))) { //checks for valid email syntax & if supplied verification code is correct
            if (!checkUserPresence(name)) { //checks if user already exists
                try {
                    Statement stm = con.createStatement(); //create statement
                    String dbop = "INSERT INTO "+JPassBackendApplication.table+" VALUES ('" + name + "', '" + key + "', NULL, 1 );";
                    stm.execute(dbop); //run previously defined command on mysql (writes user & key to db)
                    stm.close();
                    System.out.println("added user");
                    return "success!";
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("error adding user");
                    return "Internal server error!"; //returns internal server error if code breaks
                }

            } else {
                return "user already exists!"; //return error if user is already present in database
            }
        }else{
            return "invalid email or verification code";
        }

    }

    ///////////////////////////////////////////////
    //checks if username is already present in db//
    ///////////////////////////////////////////////

    //returns true if user is already present, or error occurs to prevent accidental writes
    public static Boolean checkUserPresence(String user){
        PreparedStatement pst;

        try {
            pst = con.prepareStatement("select UserNames from "+JPassBackendApplication.table+" where UserNames=? ");
            pst.setString(1, user);
            ResultSet rs1 = pst.executeQuery(); //execute SQL query
            if(rs1.next()){ //check if data returned
                //user found
                rs1.close();
                return true;
            }else if(!rs1.next()){
                //user not found
                rs1.close();
                return false;
            }
            rs1.close();
            return true;
        } catch (Exception e) {
            System.out.println("Error while fetching data from mysql! (checkUserPresence)");
            e.printStackTrace();
            return true;
        }

    }

    ///////////////////////////////////////////////
    //checks if user already requested email code//
    ///////////////////////////////////////////////

    //returns true if user is already present, or error occurs to prevent accidental writes
    public static Boolean checkUserPresenceInValidation(String email){
        PreparedStatement pst;

        try {
            pst = con.prepareStatement("select `email` from users.emailCodes where email=? ");
            pst.setString(1, email);
            ResultSet rs1 = pst.executeQuery(); //execute SQL query
            if(rs1.next()){ //check if data returned
                //user found
                rs1.close();
                return true;
            }else if(!rs1.next()){
                //user not found
                rs1.close();
                return false;
            }
            rs1.close();
            return true;
        } catch (Exception e) {
            System.out.println("Error while fetching data from mysql! (checkUserPresence)");
            e.printStackTrace();
            return true;
        }

    }

    ////////////////////////////////////////////////////
    //fetches user's security key for later validation//
    ////////////////////////////////////////////////////

    //returns user security key
    public static String getUserKey(String user){
        PreparedStatement pst;

        try {
            pst = con.prepareStatement("select `key` from "+JPassBackendApplication.table+" where UserNames=?" );
            pst.setString(1,user);
            ResultSet rs1 = pst.executeQuery(); //execute SQL query
            if(rs1.next()){ //check if data returned
                //user found
                String key = rs1.getString("key"); //write security key to string
                rs1.close();
                return key;
            }else if(!rs1.next()){
                //user not found
                rs1.close();
                return "key invalid!";
            }
            rs1.close();
            return "";
        } catch (Exception e) {
            System.out.println("Error while fetching data from mysql! (getUserKey)");
            e.printStackTrace();
            return "";
        }

    }

    //////////////////////////////////////////
    //fetches user's email verification code//
    //////////////////////////////////////////

    //returns user security key
    public static String getEmailCode(String email){
        PreparedStatement pst;

        try {
            pst = con.prepareStatement("select `code` from users.emailCodes where email=?" );
            pst.setString(1,email);
            ResultSet rs1 = pst.executeQuery(); //execute SQL query
            if(rs1.next()){ //check if data returned
                //user found
                String code = rs1.getString("code"); //write email code to string
                rs1.close();
                return code;
            }else if(!rs1.next()){
                //user not found
                rs1.close();
                return "code not found";
            }
            rs1.close();
            return "";
        } catch (Exception e) {
            System.out.println("Error while fetching data from mysql! (getEmailCode)");
            e.printStackTrace();
            return "";
        }

    }

    /////////////////////////////////////////////
    //function to add userData to existing user//
    /////////////////////////////////////////////

    //only works if checkUserPresence() returns negative and right key is supplied ==> no empty name/key fields + added security
    public static String addDataToUser(String name, String data, String key){

            if (checkUserPresence(name)) { //checks if user already exists
                if (key.equals(getUserKey(name))) { //checks if right key is supplied
                    try {
                        Statement stm = con.createStatement(); //create statement
                        String dbop = "update  users\n" +
                                "set     data = '" + data + "'\n" +
                                "where   UserNames = '" + name + "' and `key` = '" + key + "'"; //double checks key, then adds data to row
                        stm.execute(dbop); //run previously defined command on mysql (writes data to db row with corresponding name)
                        stm.close();
                        return "successfully added data to user !";
                    } catch (Exception e) {
                        e.printStackTrace();
                        return "Internal server error!"; //returns internal server error if code breaks
                    }
                } else {
                    return "key invalid!";
                }
            } else {
                return "user does not exist!"; //return error if user is already present in database
            }
    }

    public static String fetchData(String name, String key){
        String fetchedData;

        if (checkUserPresence(name)) { //checks if user already exists
            if (key.equals(getUserKey(name))) { //checks if right key is supplied
                try {
                    Statement stm = con.createStatement(); //create statement
                    String dbop = "SELECT * FROM users where UserNames = '" + name + "' and `key` = '" + key + "'";
                    ResultSet rs = stm.executeQuery(dbop); //run previously defined command on mysql (writes data to db row with corresponding name)
                    if(rs.next()) {
                        fetchedData = rs.getString("data");
                    }else{
                        fetchedData = "error fetching data!";
                    }
                    stm.close();
                    return fetchedData;
                } catch (Exception e) {
                    e.printStackTrace();
                    return "Internal server error!"; //returns internal server error if code breaks
                }
            } else {
                return "key invalid!";
            }
        } else {
            return "user does not exist!"; //return error if user is already present in database
        }
    }

}

