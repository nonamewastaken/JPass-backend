package me.nonamewastaken.JPassbackend;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.*;

public class emailSender {

    protected Session mailSession;

    public void connect(String smtpHost, String smtpPort, String userName, String password, Boolean useCreds){

        ////////////////////////////////////////////////////////////
        //Creates properties object, containing all necessary data//
        ////////////////////////////////////////////////////////////
        Properties props = new Properties();
        props.put("mail.smtp.host", smtpHost);
        props.put("mail.smtp.socketFactory.port", smtpPort);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", smtpPort);

        ///////////////////////////////////
        //authenticates with email server//
        ///////////////////////////////////
        Authenticator auth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName, password);
            }
        }; //<== This fucker belongs here, don't delete!
        this.mailSession = Session.getDefaultInstance(props, auth);
        System.out.println("Authenticated with smtp server!");
    }

    public void send(String senderMail, String senderName, String recipientAddresses, String subject, String message) throws MessagingException, UnsupportedEncodingException {
        MimeMessage msg;

        if (mailSession == null) {
            throw new IllegalStateException("Not connected to smtp server!");
        }

        msg = new MimeMessage(mailSession);

        //define necessary information to email-server
        msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
        msg.addHeader("format", "flowed");
        msg.addHeader("Content-Transfer-Encoding", "8bit");

        msg.setFrom(new InternetAddress(senderMail, senderName));
        msg.setReplyTo(InternetAddress.parse(senderMail, false));
        msg.setSubject(subject, "UTF-8");
        msg.setText(message, "UTF-8");
        msg.setSentDate(new Date());

        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientAddresses, false));
        Transport.send(msg); //sends Email
    }
}
